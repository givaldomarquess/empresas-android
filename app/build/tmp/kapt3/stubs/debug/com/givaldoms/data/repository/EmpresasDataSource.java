package com.givaldoms.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\bf\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\u0007H\'J6\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u00032\b\b\u0001\u0010\n\u001a\u00020\u00072\b\b\u0001\u0010\u000b\u001a\u00020\u00072\b\b\u0001\u0010\f\u001a\u00020\u00072\b\b\u0001\u0010\r\u001a\u00020\u0007H\'\u00a8\u0006\u000e"}, d2 = {"Lcom/givaldoms/data/repository/EmpresasDataSource;", "", "doLogin", "Lkotlinx/coroutines/experimental/Deferred;", "Lretrofit2/Response;", "Lcom/google/gson/JsonObject;", "email", "", "password", "getEnterpriseByName", "uid", "accessToken", "client", "companyName", "app_debug"})
public abstract interface EmpresasDataSource {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "users/auth/sign_in")
    @retrofit2.http.FormUrlEncoded()
    public abstract kotlinx.coroutines.experimental.Deferred<retrofit2.Response<com.google.gson.JsonObject>> doLogin(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "email")
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "password")
    java.lang.String password);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "enterprises")
    public abstract kotlinx.coroutines.experimental.Deferred<com.google.gson.JsonObject> getEnterpriseByName(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "uid")
    java.lang.String uid, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "access-token")
    java.lang.String accessToken, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "client")
    java.lang.String client, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "name")
    java.lang.String companyName);
}