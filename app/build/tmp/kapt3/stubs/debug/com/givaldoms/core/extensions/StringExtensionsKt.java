package com.givaldoms.core.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0004"}, d2 = {"isEmail", "", "", "isNotEmail", "app_debug"})
public final class StringExtensionsKt {
    
    /**
     * * Created by givaldoms on 28/05/2018.
     */
    public static final boolean isEmail(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return false;
    }
    
    public static final boolean isNotEmail(@org.jetbrains.annotations.NotNull()
    java.lang.String $receiver) {
        return false;
    }
}