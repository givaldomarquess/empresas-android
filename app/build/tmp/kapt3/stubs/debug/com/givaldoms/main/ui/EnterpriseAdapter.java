package com.givaldoms.main.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0012B\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0018\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\nH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/givaldoms/main/ui/EnterpriseAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lcom/givaldoms/main/ui/EnterpriseViewHolder;", "mItems", "", "Lcom/givaldoms/data/model/EnterpriseModel;", "adapterClickListener", "Lcom/givaldoms/main/ui/EnterpriseAdapter$OnAdapterItemClick;", "(Ljava/util/List;Lcom/givaldoms/main/ui/EnterpriseAdapter$OnAdapterItemClick;)V", "getItemCount", "", "onBindViewHolder", "", "enterpriseViewHolder", "i", "onCreateViewHolder", "viewGroup", "Landroid/view/ViewGroup;", "OnAdapterItemClick", "app_debug"})
public final class EnterpriseAdapter extends android.support.v7.widget.RecyclerView.Adapter<com.givaldoms.main.ui.EnterpriseViewHolder> {
    private final java.util.List<com.givaldoms.data.model.EnterpriseModel> mItems = null;
    private final com.givaldoms.main.ui.EnterpriseAdapter.OnAdapterItemClick adapterClickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.givaldoms.main.ui.EnterpriseViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup viewGroup, int i) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.givaldoms.main.ui.EnterpriseViewHolder enterpriseViewHolder, int i) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public EnterpriseAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<com.givaldoms.data.model.EnterpriseModel> mItems, @org.jetbrains.annotations.NotNull()
    com.givaldoms.main.ui.EnterpriseAdapter.OnAdapterItemClick adapterClickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/givaldoms/main/ui/EnterpriseAdapter$OnAdapterItemClick;", "", "onAdapterItemClickListener", "", "enterpriseModel", "Lcom/givaldoms/data/model/EnterpriseModel;", "bundle", "Landroid/os/Bundle;", "app_debug"})
    public static abstract interface OnAdapterItemClick {
        
        public abstract void onAdapterItemClickListener(@org.jetbrains.annotations.NotNull()
        com.givaldoms.data.model.EnterpriseModel enterpriseModel, @org.jetbrains.annotations.NotNull()
        android.os.Bundle bundle);
    }
}