package com.givaldoms.main.presentation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001a\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0016\u0010\u0011\u001a\u00020\f2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013H\u0002J\u0018\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010\u0019\u001a\u00020\fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u00020\u0005X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u001a"}, d2 = {"Lcom/givaldoms/main/presentation/MainPresenterImpl;", "Lcom/givaldoms/main/presentation/MainPresenter;", "empresasRepository", "Lcom/givaldoms/data/repository/EmpresasRepository;", "view", "Lcom/givaldoms/main/presentation/MainView;", "(Lcom/givaldoms/data/repository/EmpresasRepository;Lcom/givaldoms/main/presentation/MainView;)V", "getView", "()Lcom/givaldoms/main/presentation/MainView;", "setView", "(Lcom/givaldoms/main/presentation/MainView;)V", "getEnterprises", "", "userModel", "Lcom/givaldoms/data/model/UserModel;", "query", "", "handlerResult", "list", "", "Lcom/givaldoms/data/model/EnterpriseModel;", "itemClick", "enterpriseModel", "bundle", "Landroid/os/Bundle;", "stop", "app_debug"})
public final class MainPresenterImpl implements com.givaldoms.main.presentation.MainPresenter {
    private final com.givaldoms.data.repository.EmpresasRepository empresasRepository = null;
    @org.jetbrains.annotations.NotNull()
    private com.givaldoms.main.presentation.MainView view;
    
    @java.lang.Override()
    public void itemClick(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.EnterpriseModel enterpriseModel, @org.jetbrains.annotations.NotNull()
    android.os.Bundle bundle) {
    }
    
    @java.lang.Override()
    public void getEnterprises(@org.jetbrains.annotations.Nullable()
    com.givaldoms.data.model.UserModel userModel, @org.jetbrains.annotations.NotNull()
    java.lang.String query) {
    }
    
    private final void handlerResult(java.util.List<com.givaldoms.data.model.EnterpriseModel> list) {
    }
    
    @java.lang.Override()
    public void stop() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.givaldoms.main.presentation.MainView getView() {
        return null;
    }
    
    @java.lang.Override()
    public void setView(@org.jetbrains.annotations.NotNull()
    com.givaldoms.main.presentation.MainView p0) {
    }
    
    public MainPresenterImpl(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.repository.EmpresasRepository empresasRepository, @org.jetbrains.annotations.NotNull()
    com.givaldoms.main.presentation.MainView view) {
        super();
    }
}