package com.givaldoms.main.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J)\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000e0\u0012H\u0000\u00a2\u0006\u0002\b\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/givaldoms/main/ui/EnterpriseViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "containerLayout", "Landroid/support/constraint/ConstraintLayout;", "countryTextView", "Landroid/widget/TextView;", "imageView", "Landroid/widget/ImageView;", "subtitleTextView", "titleTextView", "bind", "", "enterpriseModel", "Lcom/givaldoms/data/model/EnterpriseModel;", "itemClick", "Lkotlin/Function1;", "Landroid/os/Bundle;", "bind$app_debug", "app_debug"})
public final class EnterpriseViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    private final android.support.constraint.ConstraintLayout containerLayout = null;
    private final android.widget.ImageView imageView = null;
    private final android.widget.TextView titleTextView = null;
    private final android.widget.TextView subtitleTextView = null;
    private final android.widget.TextView countryTextView = null;
    
    public final void bind$app_debug(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.EnterpriseModel enterpriseModel, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super android.os.Bundle, kotlin.Unit> itemClick) {
    }
    
    public EnterpriseViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.View itemView) {
        super(null);
    }
}