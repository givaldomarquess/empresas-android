package com.givaldoms.core.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 2, d1 = {"\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\u001e\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u001a\u0012\u0010\u0007\u001a\u00020\u0001*\u00020\b2\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"}, d2 = {"setImageUrl", "", "Landroid/widget/ImageView;", "url", "", "progressBar", "Landroid/widget/ProgressBar;", "setVisible", "Landroid/view/View;", "v", "", "app_debug"})
public final class ViewExtensionsKt {
    
    /**
     * * Created by givaldoms on 28/07/2018.
     */
    public static final void setVisible(@org.jetbrains.annotations.NotNull()
    android.view.View $receiver, boolean v) {
    }
    
    public static final void setImageUrl(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.Nullable()
    android.widget.ProgressBar progressBar) {
    }
}