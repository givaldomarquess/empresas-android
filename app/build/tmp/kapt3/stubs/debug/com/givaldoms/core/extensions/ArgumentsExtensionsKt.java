package com.givaldoms.core.extensions;

import java.lang.System;

@kotlin.Suppress(names = {"UNCHECKED_CAST"})
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 2, d1 = {"\u00002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\"\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u0003*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u001a5\u0010\u0000\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u0003*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u0001H\u0002\u00a2\u0006\u0002\u0010\b\u001a\"\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u0003*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006\u001a5\u0010\u0000\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u0003*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u0001H\u0002\u00a2\u0006\u0002\u0010\n\u001a;\u0010\u000b\u001a\u00020\u0004*\u00020\u00042*\u0010\f\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000f0\u000e0\r\"\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"argument", "Lkotlin/Lazy;", "T", "", "Landroid/support/v4/app/Fragment;", "key", "", "defaultValue", "(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/Object;)Lkotlin/Lazy;", "Landroid/support/v4/app/FragmentActivity;", "(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/Object;)Lkotlin/Lazy;", "withArguments", "arguments", "", "Lkotlin/Pair;", "Ljava/io/Serializable;", "(Landroid/support/v4/app/Fragment;[Lkotlin/Pair;)Landroid/support/v4/app/Fragment;", "app_debug"})
public final class ArgumentsExtensionsKt {
    
    /**
     * * Start an Activity for given class T and allow to work on intent with "run" lambda function
     */
    @org.jetbrains.annotations.NotNull()
    public static final android.support.v4.app.Fragment withArguments(@org.jetbrains.annotations.NotNull()
    android.support.v4.app.Fragment $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.Pair<java.lang.String, ? extends java.io.Serializable>... arguments) {
        return null;
    }
    
    /**
     * * Retrieve property from intent
     * *
     * * @throws NullPointerException
     */
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>kotlin.Lazy<T> argument(@org.jetbrains.annotations.NotNull()
    android.support.v4.app.FragmentActivity $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    /**
     * * Retrieve property with default value from intent
     */
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>kotlin.Lazy<T> argument(@org.jetbrains.annotations.NotNull()
    android.support.v4.app.FragmentActivity $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.Nullable()
    T defaultValue) {
        return null;
    }
    
    /**
     * * Retrieve property from intent
     */
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>kotlin.Lazy<T> argument(@org.jetbrains.annotations.NotNull()
    android.support.v4.app.Fragment $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    /**
     * * Retrieve property with default value from intent
     */
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>kotlin.Lazy<T> argument(@org.jetbrains.annotations.NotNull()
    android.support.v4.app.Fragment $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.Nullable()
    T defaultValue) {
        return null;
    }
}