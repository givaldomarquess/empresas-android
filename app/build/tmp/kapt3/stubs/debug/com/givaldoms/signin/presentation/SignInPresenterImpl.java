package com.givaldoms.signin.presentation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\u0018\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\b\u0010\u0015\u001a\u00020\u000fH\u0016J\b\u0010\u0016\u001a\u00020\u000fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u00020\u0005X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u0017"}, d2 = {"Lcom/givaldoms/signin/presentation/SignInPresenterImpl;", "Lcom/givaldoms/signin/presentation/SignInPresenter;", "empresasRepository", "Lcom/givaldoms/data/repository/EmpresasRepository;", "view", "Lcom/givaldoms/signin/presentation/SignInView;", "(Lcom/givaldoms/data/repository/EmpresasRepository;Lcom/givaldoms/signin/presentation/SignInView;)V", "mEmailIsValid", "", "mPasswordIsValid", "getView", "()Lcom/givaldoms/signin/presentation/SignInView;", "setView", "(Lcom/givaldoms/signin/presentation/SignInView;)V", "emailChanged", "", "email", "", "passwordChanged", "password", "signInClick", "stop", "validateLoginButtonState", "app_debug"})
public final class SignInPresenterImpl implements com.givaldoms.signin.presentation.SignInPresenter {
    private boolean mEmailIsValid;
    private boolean mPasswordIsValid;
    private final com.givaldoms.data.repository.EmpresasRepository empresasRepository = null;
    @org.jetbrains.annotations.NotNull()
    private com.givaldoms.signin.presentation.SignInView view;
    
    @java.lang.Override()
    public void stop() {
    }
    
    @java.lang.Override()
    public void signInClick(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password) {
    }
    
    @java.lang.Override()
    public void emailChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String email) {
    }
    
    @java.lang.Override()
    public void passwordChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String password) {
    }
    
    private final void validateLoginButtonState() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.givaldoms.signin.presentation.SignInView getView() {
        return null;
    }
    
    @java.lang.Override()
    public void setView(@org.jetbrains.annotations.NotNull()
    com.givaldoms.signin.presentation.SignInView p0) {
    }
    
    public SignInPresenterImpl(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.repository.EmpresasRepository empresasRepository, @org.jetbrains.annotations.NotNull()
    com.givaldoms.signin.presentation.SignInView view) {
        super();
    }
}