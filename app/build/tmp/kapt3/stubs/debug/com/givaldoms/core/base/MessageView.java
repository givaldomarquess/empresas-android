package com.givaldoms.core.base;

import java.lang.System;

/**
 * * Created by givaldoms on 28/05/2018.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J0\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00010\u000b2\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00010\u000bH\u0016J\b\u0010\r\u001a\u00020\u0007H\u0016J\u0012\u0010\u000e\u001a\u00020\u00072\b\b\u0001\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u000e\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\tH\u0016J\b\u0010\u0011\u001a\u00020\u0007H\u0016R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u00a8\u0006\u0012"}, d2 = {"Lcom/givaldoms/core/base/MessageView;", "", "mContext", "Landroid/content/Context;", "getMContext", "()Landroid/content/Context;", "showGenericAlert", "", "m", "", "okAction", "Lkotlin/Function0;", "cancelAction", "showGenericErrorMessage", "showMessage", "message", "", "showNoInternetMessage", "app_debug"})
public abstract interface MessageView {
    
    @org.jetbrains.annotations.NotNull()
    public abstract android.content.Context getMContext();
    
    public abstract void showMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String message);
    
    public abstract void showMessage(@android.support.annotation.StringRes()
    int message);
    
    public abstract void showNoInternetMessage();
    
    public abstract void showGenericErrorMessage();
    
    public abstract void showGenericAlert(@org.jetbrains.annotations.NotNull()
    java.lang.String m, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends java.lang.Object> okAction, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends java.lang.Object> cancelAction);
    
    /**
     * * Created by givaldoms on 28/05/2018.
     */
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 3)
    public final class DefaultImpls {
        
        public static void showMessage(com.givaldoms.core.base.MessageView $this, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        public static void showMessage(com.givaldoms.core.base.MessageView $this, @android.support.annotation.StringRes()
        int message) {
        }
        
        public static void showNoInternetMessage(com.givaldoms.core.base.MessageView $this) {
        }
        
        public static void showGenericErrorMessage(com.givaldoms.core.base.MessageView $this) {
        }
        
        public static void showGenericAlert(com.givaldoms.core.base.MessageView $this, @org.jetbrains.annotations.NotNull()
        java.lang.String m, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends java.lang.Object> okAction, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends java.lang.Object> cancelAction) {
        }
    }
}