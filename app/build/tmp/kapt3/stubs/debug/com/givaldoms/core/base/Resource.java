package com.givaldoms.core.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00012\u00020\u0002B\u0019\u0012\b\u0010\u0003\u001a\u0004\u0018\u00018\u0000\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u0015\u0010\u0003\u001a\u0004\u0018\u00018\u0000\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/givaldoms/core/base/Resource;", "T", "", "data", "failure", "Lcom/givaldoms/core/base/Failure;", "(Ljava/lang/Object;Lcom/givaldoms/core/base/Failure;)V", "getData", "()Ljava/lang/Object;", "Ljava/lang/Object;", "getFailure", "()Lcom/givaldoms/core/base/Failure;", "app_debug"})
public final class Resource<T extends java.lang.Object> {
    @org.jetbrains.annotations.Nullable()
    private final T data = null;
    @org.jetbrains.annotations.Nullable()
    private final com.givaldoms.core.base.Failure failure = null;
    
    @org.jetbrains.annotations.Nullable()
    public final T getData() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.givaldoms.core.base.Failure getFailure() {
        return null;
    }
    
    public Resource(@org.jetbrains.annotations.Nullable()
    T data, @org.jetbrains.annotations.Nullable()
    com.givaldoms.core.base.Failure failure) {
        super();
    }
}