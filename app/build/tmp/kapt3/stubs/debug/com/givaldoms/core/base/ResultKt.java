package com.givaldoms.core.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\'\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004\u0082\u0002\u0004\n\u0002\b\t\u00a8\u0006\u0005"}, d2 = {"awaitResult", "Lcom/givaldoms/core/base/Result;", "T", "Lkotlinx/coroutines/experimental/Deferred;", "(Lkotlinx/coroutines/experimental/Deferred;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class ResultKt {
    
    @org.jetbrains.annotations.Nullable()
    public static final <T extends java.lang.Object>java.lang.Object awaitResult(@org.jetbrains.annotations.NotNull()
    kotlinx.coroutines.experimental.Deferred<? extends T> $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.experimental.Continuation<? super com.givaldoms.core.base.Result<? extends T>> p1) {
        return null;
    }
}