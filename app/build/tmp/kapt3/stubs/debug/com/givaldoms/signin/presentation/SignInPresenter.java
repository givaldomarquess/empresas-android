package com.givaldoms.signin.presentation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\bf\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0006H&J\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006H&\u00a8\u0006\n"}, d2 = {"Lcom/givaldoms/signin/presentation/SignInPresenter;", "Lcom/givaldoms/core/base/BasePresenter;", "Lcom/givaldoms/signin/presentation/SignInView;", "emailChanged", "", "email", "", "passwordChanged", "password", "signInClick", "app_debug"})
public abstract interface SignInPresenter extends com.givaldoms.core.base.BasePresenter<com.givaldoms.signin.presentation.SignInView> {
    
    public abstract void emailChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String email);
    
    public abstract void passwordChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String password);
    
    public abstract void signInClick(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password);
}