package com.givaldoms.data.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\"\u001b\u0010\u0000\u001a\f\u0012\u0004\u0012\u00020\u00020\u0001j\u0002`\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005\u00a8\u0006\u0006"}, d2 = {"repositoryModule", "Lkotlin/Function0;", "Lorg/koin/dsl/context/Context;", "Lorg/koin/dsl/module/Module;", "getRepositoryModule", "()Lkotlin/jvm/functions/Function0;", "app_debug"})
public final class RepositoryModuleKt {
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.jvm.functions.Function0<org.koin.dsl.context.Context> repositoryModule = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final kotlin.jvm.functions.Function0<org.koin.dsl.context.Context> getRepositoryModule() {
        return null;
    }
}