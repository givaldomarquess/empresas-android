package com.givaldoms.core.base;

import java.lang.System;

/**
 * * Created by givaldoms on 28/05/2018.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\bf\u0018\u0000*\u000e\b\u0000\u0010\u0001 \u0001*\u0006\u0012\u0002\b\u00030\u00022\u00020\u0003R\u0012\u0010\u0004\u001a\u00028\u0000X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/givaldoms/core/base/BaseView;", "T", "Lcom/givaldoms/core/base/BasePresenter;", "", "presenter", "getPresenter", "()Lcom/givaldoms/core/base/BasePresenter;", "app_debug"})
public abstract interface BaseView<T extends com.givaldoms.core.base.BasePresenter<?>> {
    
    @org.jetbrains.annotations.NotNull()
    public abstract T getPresenter();
}