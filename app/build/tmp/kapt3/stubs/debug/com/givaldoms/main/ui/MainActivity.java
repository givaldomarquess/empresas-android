package com.givaldoms.main.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0017\u001a\u00020\u00182\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aH\u0016J\b\u0010\u001c\u001a\u00020\u0018H\u0016J\u0018\u0010\u001d\u001a\u00020\u00182\u0006\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0012\u0010!\u001a\u00020\u00182\b\u0010\"\u001a\u0004\u0018\u00010 H\u0014J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0016J\u0010\u0010\'\u001a\u00020$2\u0006\u0010(\u001a\u00020)H\u0016J\b\u0010*\u001a\u00020\u0018H\u0014J\b\u0010+\u001a\u00020\u0018H\u0016J\b\u0010,\u001a\u00020\u0018H\u0016J\b\u0010-\u001a\u00020\u0018H\u0016J\u0018\u0010.\u001a\u00020\u00182\u0006\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020 H\u0016J\b\u0010/\u001a\u00020\u0018H\u0016R\u0014\u0010\u0005\u001a\u00020\u0000X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u001d\u0010\f\u001a\u0004\u0018\u00010\r8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0012\u001a\u00020\u00138VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0016\u0010\u0011\u001a\u0004\b\u0014\u0010\u0015\u00a8\u00060"}, d2 = {"Lcom/givaldoms/main/ui/MainActivity;", "Landroid/support/v7/app/AppCompatActivity;", "Lcom/givaldoms/main/presentation/MainView;", "Lcom/givaldoms/main/ui/EnterpriseAdapter$OnAdapterItemClick;", "()V", "mContext", "getMContext", "()Lcom/givaldoms/main/ui/MainActivity;", "mEnterpriseAdapter", "Lcom/givaldoms/main/ui/EnterpriseAdapter;", "mSearchView", "Landroid/support/v7/widget/SearchView;", "mUserModel", "Lcom/givaldoms/data/model/UserModel;", "getMUserModel", "()Lcom/givaldoms/data/model/UserModel;", "mUserModel$delegate", "Lkotlin/Lazy;", "presenter", "Lcom/givaldoms/main/presentation/MainPresenter;", "getPresenter", "()Lcom/givaldoms/main/presentation/MainPresenter;", "presenter$delegate", "addResults", "", "items", "", "Lcom/givaldoms/data/model/EnterpriseModel;", "hideProgress", "onAdapterItemClickListener", "enterpriseModel", "bundle", "Landroid/os/Bundle;", "onCreate", "savedInstanceState", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onStart", "refreshResult", "showNoResultMessage", "showProgress", "toEnterpriseDetail", "toLogin", "app_debug"})
public final class MainActivity extends android.support.v7.app.AppCompatActivity implements com.givaldoms.main.presentation.MainView, com.givaldoms.main.ui.EnterpriseAdapter.OnAdapterItemClick {
    private final kotlin.Lazy mUserModel$delegate = null;
    private android.support.v7.widget.SearchView mSearchView;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy presenter$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final com.givaldoms.main.ui.MainActivity mContext = null;
    private com.givaldoms.main.ui.EnterpriseAdapter mEnterpriseAdapter;
    private java.util.HashMap _$_findViewCache;
    
    private final com.givaldoms.data.model.UserModel getMUserModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.givaldoms.main.presentation.MainPresenter getPresenter() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.givaldoms.main.ui.MainActivity getMContext() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void onAdapterItemClickListener(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.EnterpriseModel enterpriseModel, @org.jetbrains.annotations.NotNull()
    android.os.Bundle bundle) {
    }
    
    @java.lang.Override()
    public void showProgress() {
    }
    
    @java.lang.Override()
    public void hideProgress() {
    }
    
    @java.lang.Override()
    public void addResults(@org.jetbrains.annotations.NotNull()
    java.util.List<com.givaldoms.data.model.EnterpriseModel> items) {
    }
    
    @java.lang.Override()
    public void showNoResultMessage() {
    }
    
    @java.lang.Override()
    public void refreshResult() {
    }
    
    @java.lang.Override()
    public void toEnterpriseDetail(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.EnterpriseModel enterpriseModel, @org.jetbrains.annotations.NotNull()
    android.os.Bundle bundle) {
    }
    
    @java.lang.Override()
    public void toLogin() {
    }
    
    public MainActivity() {
        super();
    }
    
    @java.lang.Override()
    public void showGenericAlert(@org.jetbrains.annotations.NotNull()
    java.lang.String m, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends java.lang.Object> okAction, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends java.lang.Object> cancelAction) {
    }
    
    @java.lang.Override()
    public void showGenericErrorMessage() {
    }
    
    @java.lang.Override()
    public void showMessage(@android.support.annotation.StringRes()
    int message) {
    }
    
    @java.lang.Override()
    public void showMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void showNoInternetMessage() {
    }
}