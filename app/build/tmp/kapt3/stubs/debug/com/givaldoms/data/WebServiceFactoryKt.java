package com.givaldoms.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u0006\u0010\u0006\u001a\u00020\u0007\u001a.\u0010\b\u001a\n \n*\u0004\u0018\u0001H\tH\t\"\u0006\b\u0000\u0010\t\u0018\u00012\u0006\u0010\u000b\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\rH\u0086\b\u00a2\u0006\u0002\u0010\u000e\"\u001b\u0010\u0000\u001a\f\u0012\u0004\u0012\u00020\u00020\u0001j\u0002`\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005\u00a8\u0006\u000f"}, d2 = {"remoteDataSourceModule", "Lkotlin/Function0;", "Lorg/koin/dsl/context/Context;", "Lorg/koin/dsl/module/Module;", "getRemoteDataSourceModule", "()Lkotlin/jvm/functions/Function0;", "createOkHttpClient", "Lokhttp3/OkHttpClient;", "createWebService", "T", "kotlin.jvm.PlatformType", "okHttpClient", "url", "", "(Lokhttp3/OkHttpClient;Ljava/lang/String;)Ljava/lang/Object;", "app_debug"})
public final class WebServiceFactoryKt {
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.jvm.functions.Function0<org.koin.dsl.context.Context> remoteDataSourceModule = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final kotlin.jvm.functions.Function0<org.koin.dsl.context.Context> getRemoteDataSourceModule() {
        return null;
    }
    
    private static final <T extends java.lang.Object>T createWebService(okhttp3.OkHttpClient okHttpClient, java.lang.String url) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final okhttp3.OkHttpClient createOkHttpClient() {
        return null;
    }
}