package com.givaldoms.core.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 2, d1 = {"\u00000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u001e\u0010\u0003\u001a\u00020\u0001*\u00020\u00022\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u001a-\u0010\b\u001a\u00020\u0001*\u00020\t2!\u0010\u0004\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\r0\u0005\u001a-\u0010\u000e\u001a\u00020\u0001*\u00020\t2!\u0010\u0004\u001a\u001d\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\r0\u0005\u001a \u0010\u000f\u001a\u00020\u0001*\u00020\u00022\u0014\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0005\u001a\u0012\u0010\u0010\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u0007\u00a8\u0006\u0012"}, d2 = {"moveCursorToEnd", "", "Landroid/widget/EditText;", "onActionDone", "f", "Lkotlin/Function1;", "", "", "onQueryTextChangeListener", "Landroid/support/v7/widget/SearchView;", "Lkotlin/ParameterName;", "name", "query", "", "onQueryTextSubmit", "onTextChanged", "putText", "text", "app_debug"})
public final class EditTextExtensionsKt {
    
    /**
     * * Created by givaldoms on 28/05/2018.
     */
    public static final void putText(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $receiver, @org.jetbrains.annotations.NotNull()
    java.lang.Object text) {
    }
    
    public static final void onTextChanged(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, ? extends java.lang.Object> f) {
    }
    
    public static final void onActionDone(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, ? extends java.lang.Object> f) {
    }
    
    public static final void moveCursorToEnd(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $receiver) {
    }
    
    public static final void onQueryTextChangeListener(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.SearchView $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, java.lang.Boolean> f) {
    }
    
    public static final void onQueryTextSubmit(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.SearchView $receiver, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, java.lang.Boolean> f) {
    }
}