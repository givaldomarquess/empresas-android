package com.givaldoms.signin.presentation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u00032\u00020\u0004J\b\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\u0006H&J\b\u0010\b\u001a\u00020\u0006H&J\b\u0010\t\u001a\u00020\u0006H&J\b\u0010\n\u001a\u00020\u0006H&J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\rH&J\b\u0010\u000e\u001a\u00020\u0006H&J\b\u0010\u000f\u001a\u00020\u0006H&J\b\u0010\u0010\u001a\u00020\u0006H&J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0013H&\u00a8\u0006\u0014"}, d2 = {"Lcom/givaldoms/signin/presentation/SignInView;", "Lcom/givaldoms/core/base/BaseView;", "Lcom/givaldoms/signin/presentation/SignInPresenter;", "Lcom/givaldoms/core/base/LoadingView;", "Lcom/givaldoms/core/base/MessageView;", "clearEmailErrors", "", "clearPasswordErrors", "setEmailEmptyError", "setEmailIncorrectError", "setEmailInvalidError", "setLoginButtonEnabled", "state", "", "setPasswordEmptyError", "setPasswordIncorrectError", "setPasswordInvalidError", "toMain", "userModel", "Lcom/givaldoms/data/model/UserModel;", "app_debug"})
public abstract interface SignInView extends com.givaldoms.core.base.BaseView<com.givaldoms.signin.presentation.SignInPresenter>, com.givaldoms.core.base.LoadingView, com.givaldoms.core.base.MessageView {
    
    public abstract void setEmailInvalidError();
    
    public abstract void setEmailEmptyError();
    
    public abstract void setEmailIncorrectError();
    
    public abstract void clearEmailErrors();
    
    public abstract void setPasswordEmptyError();
    
    public abstract void setPasswordInvalidError();
    
    public abstract void setPasswordIncorrectError();
    
    public abstract void clearPasswordErrors();
    
    public abstract void setLoginButtonEnabled(boolean state);
    
    public abstract void toMain(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.UserModel userModel);
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 3)
    public final class DefaultImpls {
        
        @java.lang.Override()
        public static void showMessage(com.givaldoms.signin.presentation.SignInView $this, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        @java.lang.Override()
        public static void showMessage(com.givaldoms.signin.presentation.SignInView $this, @android.support.annotation.StringRes()
        int message) {
        }
        
        @java.lang.Override()
        public static void showNoInternetMessage(com.givaldoms.signin.presentation.SignInView $this) {
        }
        
        @java.lang.Override()
        public static void showGenericErrorMessage(com.givaldoms.signin.presentation.SignInView $this) {
        }
        
        @java.lang.Override()
        public static void showGenericAlert(com.givaldoms.signin.presentation.SignInView $this, @org.jetbrains.annotations.NotNull()
        java.lang.String m, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends java.lang.Object> okAction, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends java.lang.Object> cancelAction) {
        }
    }
}