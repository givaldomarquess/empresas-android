package com.givaldoms.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\'\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ-\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n2\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0006H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000f\u0082\u0002\u0004\n\u0002\b\t\u00a8\u0006\u0010"}, d2 = {"Lcom/givaldoms/data/repository/EmpresasRepository;", "", "doLogin", "Lcom/givaldoms/core/base/Resource;", "Lcom/givaldoms/data/model/UserModel;", "email", "", "password", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "searchEnterpriseByName", "Lcom/givaldoms/core/base/Result;", "", "Lcom/givaldoms/data/model/EnterpriseModel;", "userModel", "nameQuery", "(Lcom/givaldoms/data/model/UserModel;Ljava/lang/String;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface EmpresasRepository {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object searchEnterpriseByName(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.UserModel userModel, @org.jetbrains.annotations.NotNull()
    java.lang.String nameQuery, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.experimental.Continuation<? super com.givaldoms.core.base.Result<? extends java.util.List<com.givaldoms.data.model.EnterpriseModel>>> p2);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object doLogin(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.experimental.Continuation<? super com.givaldoms.core.base.Resource<com.givaldoms.data.model.UserModel>> p2);
}