package com.givaldoms.signin.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 2, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u001b\u0010\u0002\u001a\f\u0012\u0004\u0012\u00020\u00040\u0003j\u0002`\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"SIGN_IN_VIEW", "", "signInModule", "Lkotlin/Function0;", "Lorg/koin/dsl/context/Context;", "Lorg/koin/dsl/module/Module;", "getSignInModule", "()Lkotlin/jvm/functions/Function0;", "app_debug"})
public final class SignInModuleKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SIGN_IN_VIEW = "sign_in_view";
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.jvm.functions.Function0<org.koin.dsl.context.Context> signInModule = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final kotlin.jvm.functions.Function0<org.koin.dsl.context.Context> getSignInModule() {
        return null;
    }
}