package com.givaldoms.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ-\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\r2\u0006\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\tH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\t\u00a8\u0006\u0013"}, d2 = {"Lcom/givaldoms/data/repository/EmpresasRepositoryImpl;", "Lcom/givaldoms/data/repository/EmpresasRepository;", "empresasDataSource", "Lcom/givaldoms/data/repository/EmpresasDataSource;", "(Lcom/givaldoms/data/repository/EmpresasDataSource;)V", "doLogin", "Lcom/givaldoms/core/base/Resource;", "Lcom/givaldoms/data/model/UserModel;", "email", "", "password", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "searchEnterpriseByName", "Lcom/givaldoms/core/base/Result;", "", "Lcom/givaldoms/data/model/EnterpriseModel;", "userModel", "nameQuery", "(Lcom/givaldoms/data/model/UserModel;Ljava/lang/String;Lkotlin/coroutines/experimental/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class EmpresasRepositoryImpl implements com.givaldoms.data.repository.EmpresasRepository {
    private final com.givaldoms.data.repository.EmpresasDataSource empresasDataSource = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object doLogin(@org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.experimental.Continuation<? super com.givaldoms.core.base.Resource<com.givaldoms.data.model.UserModel>> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object searchEnterpriseByName(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.UserModel userModel, @org.jetbrains.annotations.NotNull()
    java.lang.String nameQuery, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.experimental.Continuation<? super com.givaldoms.core.base.Result<? extends java.util.List<com.givaldoms.data.model.EnterpriseModel>>> p2) {
        return null;
    }
    
    public EmpresasRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.repository.EmpresasDataSource empresasDataSource) {
        super();
    }
}