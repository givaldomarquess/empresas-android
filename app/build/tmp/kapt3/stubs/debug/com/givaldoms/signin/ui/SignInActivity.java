package com.givaldoms.signin.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u000eH\u0016J\b\u0010\u0010\u001a\u00020\u000eH\u0016J\u0012\u0010\u0011\u001a\u00020\u000e2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0014J\b\u0010\u0014\u001a\u00020\u000eH\u0014J\b\u0010\u0015\u001a\u00020\u000eH\u0014J\b\u0010\u0016\u001a\u00020\u000eH\u0016J\b\u0010\u0017\u001a\u00020\u000eH\u0016J\b\u0010\u0018\u001a\u00020\u000eH\u0016J\u0010\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u000eH\u0016J\b\u0010\u001d\u001a\u00020\u000eH\u0016J\b\u0010\u001e\u001a\u00020\u000eH\u0016J\b\u0010\u001f\u001a\u00020\u000eH\u0016J\u0010\u0010 \u001a\u00020\u000e2\u0006\u0010!\u001a\u00020\"H\u0016R\u0014\u0010\u0004\u001a\u00020\u0000X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\u0007\u001a\u00020\b8VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\n\u00a8\u0006#"}, d2 = {"Lcom/givaldoms/signin/ui/SignInActivity;", "Landroid/support/v7/app/AppCompatActivity;", "Lcom/givaldoms/signin/presentation/SignInView;", "()V", "mContext", "getMContext", "()Lcom/givaldoms/signin/ui/SignInActivity;", "presenter", "Lcom/givaldoms/signin/presentation/SignInPresenter;", "getPresenter", "()Lcom/givaldoms/signin/presentation/SignInPresenter;", "presenter$delegate", "Lkotlin/Lazy;", "clearEmailErrors", "", "clearPasswordErrors", "hideProgress", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onStart", "setEmailEmptyError", "setEmailIncorrectError", "setEmailInvalidError", "setLoginButtonEnabled", "state", "", "setPasswordEmptyError", "setPasswordIncorrectError", "setPasswordInvalidError", "showProgress", "toMain", "userModel", "Lcom/givaldoms/data/model/UserModel;", "app_debug"})
public final class SignInActivity extends android.support.v7.app.AppCompatActivity implements com.givaldoms.signin.presentation.SignInView {
    @org.jetbrains.annotations.NotNull()
    private final com.givaldoms.signin.ui.SignInActivity mContext = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy presenter$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.givaldoms.signin.ui.SignInActivity getMContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.givaldoms.signin.presentation.SignInPresenter getPresenter() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    public void showProgress() {
    }
    
    @java.lang.Override()
    public void hideProgress() {
    }
    
    @java.lang.Override()
    public void setEmailInvalidError() {
    }
    
    @java.lang.Override()
    public void setEmailEmptyError() {
    }
    
    @java.lang.Override()
    public void setEmailIncorrectError() {
    }
    
    @java.lang.Override()
    public void clearEmailErrors() {
    }
    
    @java.lang.Override()
    public void setPasswordEmptyError() {
    }
    
    @java.lang.Override()
    public void setPasswordInvalidError() {
    }
    
    @java.lang.Override()
    public void setPasswordIncorrectError() {
    }
    
    @java.lang.Override()
    public void clearPasswordErrors() {
    }
    
    @java.lang.Override()
    public void setLoginButtonEnabled(boolean state) {
    }
    
    @java.lang.Override()
    public void toMain(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.UserModel userModel) {
    }
    
    public SignInActivity() {
        super();
    }
    
    @java.lang.Override()
    public void showGenericAlert(@org.jetbrains.annotations.NotNull()
    java.lang.String m, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends java.lang.Object> okAction, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends java.lang.Object> cancelAction) {
    }
    
    @java.lang.Override()
    public void showGenericErrorMessage() {
    }
    
    @java.lang.Override()
    public void showMessage(@android.support.annotation.StringRes()
    int message) {
    }
    
    @java.lang.Override()
    public void showMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void showNoInternetMessage() {
    }
}