package com.givaldoms.core;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/givaldoms/core/Arguments;", "", "()V", "ARG_ENTERPRISE_MODEL", "", "ARG_USER_MODEL", "app_debug"})
public final class Arguments {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ARG_USER_MODEL = "arguments_user_model";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ARG_ENTERPRISE_MODEL = "argumentos_enterprise_model";
    public static final com.givaldoms.core.Arguments INSTANCE = null;
    
    private Arguments() {
        super();
    }
}