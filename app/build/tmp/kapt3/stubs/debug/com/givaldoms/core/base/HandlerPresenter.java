package com.givaldoms.core.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u00020\u00032\n\u0010\u0004\u001a\u00060\u0005j\u0002`\u0006H&\u00a8\u0006\u0007"}, d2 = {"Lcom/givaldoms/core/base/HandlerPresenter;", "", "handlerException", "", "ex", "Ljava/lang/Exception;", "Lkotlin/Exception;", "app_debug"})
public abstract interface HandlerPresenter {
    
    public abstract void handlerException(@org.jetbrains.annotations.NotNull()
    java.lang.Exception ex);
}