package com.givaldoms.core.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u000eB\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J!\u0010\u0007\u001a\u00020\u00032\u0012\u0010\b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00020\t\"\u00020\u0002H\u0014\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0003H\u0014R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/givaldoms/core/extensions/InternetCheck;", "Landroid/os/AsyncTask;", "Ljava/lang/Void;", "", "mConsumer", "Lcom/givaldoms/core/extensions/InternetCheck$Consumer;", "(Lcom/givaldoms/core/extensions/InternetCheck$Consumer;)V", "doInBackground", "voids", "", "([Ljava/lang/Void;)Ljava/lang/Boolean;", "onPostExecute", "", "internet", "Consumer", "app_debug"})
public final class InternetCheck extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Boolean> {
    private final com.givaldoms.core.extensions.InternetCheck.Consumer mConsumer = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected java.lang.Boolean doInBackground(@org.jetbrains.annotations.NotNull()
    java.lang.Void... voids) {
        return null;
    }
    
    @java.lang.Override()
    protected void onPostExecute(boolean internet) {
    }
    
    public InternetCheck(@org.jetbrains.annotations.NotNull()
    com.givaldoms.core.extensions.InternetCheck.Consumer mConsumer) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/givaldoms/core/extensions/InternetCheck$Consumer;", "", "accept", "", "internet", "", "app_debug"})
    public static abstract interface Consumer {
        
        public abstract void accept(boolean internet);
    }
}