package com.givaldoms.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u0007J\u0006\u0010\b\u001a\u00020\t\u00a8\u0006\n"}, d2 = {"Lcom/givaldoms/data/DumbData;", "", "()V", "dumbEnterpriseList", "", "Lcom/givaldoms/data/model/EnterpriseModel;", "size", "", "dumbUserModel", "Lcom/givaldoms/data/model/UserModel;", "app_debug"})
public final class DumbData {
    public static final com.givaldoms.data.DumbData INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.givaldoms.data.model.EnterpriseModel> dumbEnterpriseList(int size) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.givaldoms.data.model.UserModel dumbUserModel() {
        return null;
    }
    
    private DumbData() {
        super();
    }
}