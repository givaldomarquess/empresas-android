package com.givaldoms.core;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/givaldoms/core/Constants;", "", "()V", "RESULT_CODE_BAD_REQUEST", "", "RESULT_CODE_UNAUTHORIZED", "WEB_SERVICE_BASE_URL", "", "app_debug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WEB_SERVICE_BASE_URL = "http://54.94.179.135:8090";
    public static final int RESULT_CODE_UNAUTHORIZED = 401;
    public static final int RESULT_CODE_BAD_REQUEST = 400;
    public static final com.givaldoms.core.Constants INSTANCE = null;
    
    private Constants() {
        super();
    }
}