package com.givaldoms.main.presentation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u00032\u00020\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH&J\b\u0010\n\u001a\u00020\u0006H&J\b\u0010\u000b\u001a\u00020\u0006H&J\u0018\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000fH&J\b\u0010\u0010\u001a\u00020\u0006H&\u00a8\u0006\u0011"}, d2 = {"Lcom/givaldoms/main/presentation/MainView;", "Lcom/givaldoms/core/base/BaseView;", "Lcom/givaldoms/main/presentation/MainPresenter;", "Lcom/givaldoms/core/base/LoadingView;", "Lcom/givaldoms/core/base/MessageView;", "addResults", "", "items", "", "Lcom/givaldoms/data/model/EnterpriseModel;", "refreshResult", "showNoResultMessage", "toEnterpriseDetail", "enterpriseModel", "bundle", "Landroid/os/Bundle;", "toLogin", "app_debug"})
public abstract interface MainView extends com.givaldoms.core.base.BaseView<com.givaldoms.main.presentation.MainPresenter>, com.givaldoms.core.base.LoadingView, com.givaldoms.core.base.MessageView {
    
    public abstract void showNoResultMessage();
    
    public abstract void addResults(@org.jetbrains.annotations.NotNull()
    java.util.List<com.givaldoms.data.model.EnterpriseModel> items);
    
    public abstract void refreshResult();
    
    public abstract void toEnterpriseDetail(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.EnterpriseModel enterpriseModel, @org.jetbrains.annotations.NotNull()
    android.os.Bundle bundle);
    
    public abstract void toLogin();
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 3)
    public final class DefaultImpls {
        
        @java.lang.Override()
        public static void showGenericAlert(com.givaldoms.main.presentation.MainView $this, @org.jetbrains.annotations.NotNull()
        java.lang.String m, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends java.lang.Object> okAction, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends java.lang.Object> cancelAction) {
        }
        
        @java.lang.Override()
        public static void showGenericErrorMessage(com.givaldoms.main.presentation.MainView $this) {
        }
        
        @java.lang.Override()
        public static void showMessage(com.givaldoms.main.presentation.MainView $this, @android.support.annotation.StringRes()
        int message) {
        }
        
        @java.lang.Override()
        public static void showMessage(com.givaldoms.main.presentation.MainView $this, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        @java.lang.Override()
        public static void showNoInternetMessage(com.givaldoms.main.presentation.MainView $this) {
        }
    }
}