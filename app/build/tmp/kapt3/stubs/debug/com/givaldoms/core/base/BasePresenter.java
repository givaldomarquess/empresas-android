package com.givaldoms.core.base;

import java.lang.System;

/**
 * * Created by givaldoms on 28/05/2018.
 */
@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\bf\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002J\b\u0010\b\u001a\u00020\tH&R\u0018\u0010\u0003\u001a\u00028\u0000X\u00a6\u000e\u00a2\u0006\f\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007\u00a8\u0006\n"}, d2 = {"Lcom/givaldoms/core/base/BasePresenter;", "V", "", "view", "getView", "()Ljava/lang/Object;", "setView", "(Ljava/lang/Object;)V", "stop", "", "app_debug"})
public abstract interface BasePresenter<V extends java.lang.Object> {
    
    public abstract V getView();
    
    public abstract void setView(V p0);
    
    public abstract void stop();
}