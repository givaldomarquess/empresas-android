package com.givaldoms.main.presentation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u001a\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH&J\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH&\u00a8\u0006\u000e"}, d2 = {"Lcom/givaldoms/main/presentation/MainPresenter;", "Lcom/givaldoms/core/base/BasePresenter;", "Lcom/givaldoms/main/presentation/MainView;", "getEnterprises", "", "userModel", "Lcom/givaldoms/data/model/UserModel;", "query", "", "itemClick", "enterpriseModel", "Lcom/givaldoms/data/model/EnterpriseModel;", "bundle", "Landroid/os/Bundle;", "app_debug"})
public abstract interface MainPresenter extends com.givaldoms.core.base.BasePresenter<com.givaldoms.main.presentation.MainView> {
    
    public abstract void itemClick(@org.jetbrains.annotations.NotNull()
    com.givaldoms.data.model.EnterpriseModel enterpriseModel, @org.jetbrains.annotations.NotNull()
    android.os.Bundle bundle);
    
    public abstract void getEnterprises(@org.jetbrains.annotations.Nullable()
    com.givaldoms.data.model.UserModel userModel, @org.jetbrains.annotations.NotNull()
    java.lang.String query);
}