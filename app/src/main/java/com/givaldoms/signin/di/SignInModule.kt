package com.givaldoms.signin.di

import com.givaldoms.signin.presentation.SignInPresenter
import com.givaldoms.signin.presentation.SignInPresenterImpl
import org.koin.dsl.module.applicationContext

const val SIGN_IN_VIEW = "sign_in_view"

val signInModule = applicationContext {

    factory { SignInPresenterImpl(get(), it[SIGN_IN_VIEW]) as SignInPresenter }
}

