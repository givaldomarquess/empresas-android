package com.givaldoms.signin.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.givaldoms.R
import com.givaldoms.core.Arguments
import com.givaldoms.core.extensions.onActionDone
import com.givaldoms.core.extensions.onTextChanged
import com.givaldoms.data.model.UserModel
import com.givaldoms.main.ui.MainActivity
import com.givaldoms.signin.di.SIGN_IN_VIEW
import com.givaldoms.signin.presentation.SignInPresenter
import com.givaldoms.signin.presentation.SignInView
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.sdk25.coroutines.onFocusChange
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject

class SignInActivity : AppCompatActivity(), SignInView {

    override val mContext = this

    override val presenter: SignInPresenter by inject {
        mapOf(SIGN_IN_VIEW to this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
    }

    override fun onStart() {
        super.onStart()

        signInLoginButton.setOnClickListener {
            val email = signInEmailEditText.text?.toString()
            val password = signInPasswordEditText.text?.toString()
            presenter.signInClick(email ?: "", password ?: "")
        }

        signInEmailEditText.onTextChanged {
            presenter.emailChanged(it)
        }

        signInPasswordEditText.onTextChanged {
            presenter.passwordChanged(it)
        }

        signInPasswordEditText.onActionDone {
            signInLoginButton.performClick()
        }

    }

    override fun onDestroy() {
        presenter.stop()
        super.onDestroy()
    }

    override fun showProgress() {
        signInLoginProgressBar.visibility = View.VISIBLE
        signInLoginButton.isEnabled = false
    }

    override fun hideProgress() {
        signInLoginProgressBar.visibility = View.GONE
        signInLoginButton.isEnabled = true
    }


    override fun setEmailInvalidError() {
        signInEmailInputLayout?.error = getString(R.string.error_message_invalid_email)
    }

    override fun setEmailEmptyError() {
        signInEmailInputLayout?.error = getString(R.string.error_message_empty_field)
    }

    override fun setEmailIncorrectError() {
        signInEmailInputLayout?.error = getString(R.string.error_message_incorrect_email)
    }

    override fun clearEmailErrors() {
        signInEmailInputLayout?.error = null
    }

    override fun setPasswordEmptyError() {
        signInPasswordInputLayout?.error = getString(R.string.error_message_empty_field)
    }

    override fun setPasswordInvalidError() {
        signInPasswordInputLayout?.error = getString(R.string.error_message_invalid_password)
    }

    override fun setPasswordIncorrectError() {
        signInPasswordInputLayout?.error = getString(R.string.error_message_incorrect_password)
    }

    override fun clearPasswordErrors() {
        signInPasswordInputLayout?.error = null
    }

    override fun setLoginButtonEnabled(state: Boolean) {
        signInLoginButton?.isEnabled = state
    }

    override fun toMain(userModel: UserModel) {
        startActivity<MainActivity>(Arguments.ARG_USER_MODEL to userModel)
        finishAffinity()
    }
}
