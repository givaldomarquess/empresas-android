package com.givaldoms.signin.presentation

import com.givaldoms.core.extensions.isNotEmail
import com.givaldoms.core.extensions.withInternetStatus
import com.givaldoms.data.repository.EmpresasRepository
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch

class SignInPresenterImpl(private val empresasRepository: EmpresasRepository,
                          override var view: SignInView) : SignInPresenter {

    private var mEmailIsValid = false
    private var mPasswordIsValid = false

    override fun stop() {
    }

    override fun signInClick(email: String, password: String) = withInternetStatus { isOnline ->

        view.clearEmailErrors()
        view.clearPasswordErrors()

        if (!isOnline) {
            view.showNoInternetMessage()
            return@withInternetStatus
        }

        view.showProgress()

        launch(UI) {

            val resource = async {
                empresasRepository.doLogin(email.trim().toLowerCase(),
                        password.trim())
            }.await()

            when {
                resource.data != null -> view.toMain(resource.data)
                resource.failure != null && resource.failure.isUnauthorized() ->
                    view.setPasswordIncorrectError()
                else -> view.showGenericErrorMessage()
            }
            view.hideProgress()
        }


    }

    override fun emailChanged(email: String) {
        val e = email.trim().toLowerCase()

        mEmailIsValid = when {
            e.isBlank() -> {
                view.setEmailEmptyError()
                false

            }
            e.isNotEmail() -> {
                view.setEmailInvalidError()
                false

            }
            else -> {
                view.clearEmailErrors()
                true
            }
        }

        validateLoginButtonState()
    }

    override fun passwordChanged(password: String) {
        val p = password.trim()

        mPasswordIsValid = when {
            p.isBlank() -> {
                view.setPasswordEmptyError()
                false
            }

            p.length < 6 -> {
                view.setPasswordInvalidError()
                false
            }

            else -> {
                view.clearPasswordErrors()
                true
            }
        }
        validateLoginButtonState()
    }

    private fun validateLoginButtonState() {
        view.setLoginButtonEnabled(mEmailIsValid && mPasswordIsValid)
    }

}