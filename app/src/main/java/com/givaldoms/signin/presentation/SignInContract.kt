package com.givaldoms.signin.presentation

import com.givaldoms.core.base.BasePresenter
import com.givaldoms.core.base.BaseView
import com.givaldoms.core.base.LoadingView
import com.givaldoms.core.base.MessageView
import com.givaldoms.data.model.UserModel

interface SignInPresenter: BasePresenter<SignInView> {

    fun emailChanged(email: String)

    fun passwordChanged(password: String)

    fun signInClick(email: String, password: String)

}

interface SignInView: BaseView<SignInPresenter>, LoadingView, MessageView {

    fun setEmailInvalidError()

    fun setEmailEmptyError()

    fun setEmailIncorrectError()

    fun clearEmailErrors()

    fun setPasswordEmptyError()

    fun setPasswordInvalidError()

    fun setPasswordIncorrectError()

    fun clearPasswordErrors()

    fun setLoginButtonEnabled(state: Boolean)

    fun toMain(userModel: UserModel)
}