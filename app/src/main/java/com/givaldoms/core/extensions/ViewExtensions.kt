package com.givaldoms.core.extensions

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.givaldoms.R
import com.givaldoms.core.GlideApp

/**
 * Created by givaldoms on 28/07/2018.
 */

fun View.setVisible(v: Boolean) {
    if (v) this.visibility = View.VISIBLE
    else this.visibility = View.GONE
}

fun ImageView.setImageUrl(url: String, progressBar: ProgressBar? = null) {
    progressBar?.setVisible(true)

    GlideApp.with(context)
            .load(url)
            .error(R.drawable.imb_dumb_sample)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?,
                                          i0: Boolean): Boolean {
                    progressBar?.setVisible(false)
                    return false
                }

                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    progressBar?.setVisible(false)
                    return false
                }

            })
            .into(this)
}