package com.givaldoms.core.extensions

import android.support.v7.widget.SearchView
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import android.widget.EditText

/**
 * Created by givaldoms on 28/05/2018.
 */

fun EditText.putText(text: Any) {
    this.text = Editable.Factory.getInstance().newEditable(text.toString())
}

fun EditText.onTextChanged(f: (String) -> Any?) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            f(s?.toString() ?: "")
        }
    })
}

fun EditText.onActionDone(f: (String) -> Any) {
    this.setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            f(this.text.toString())
        }
        false
    }

}

fun EditText.moveCursorToEnd() {
    this.setSelection(this.text.length)
}



fun SearchView.onQueryTextChangeListener(f: (query: String) -> Boolean) {
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(s: String): Boolean {
            return false
        }

        override fun onQueryTextChange(s: String): Boolean {
            return f(s)
        }
    })
}


fun SearchView.onQueryTextSubmit(f: (query: String) -> Boolean) {
    val searchView = this
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(s: String): Boolean {
            searchView.clearFocus()
            return f(s)
        }

        override fun onQueryTextChange(s: String): Boolean {
            return false
        }
    })
}