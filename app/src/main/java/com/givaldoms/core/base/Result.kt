package com.givaldoms.core.base

import com.givaldoms.core.Constants.RESULT_CODE_UNAUTHORIZED
import kotlinx.coroutines.experimental.Deferred

suspend fun <T> Deferred<T>.awaitResult(): Result<T> = try {
    Result(await(), null)
} catch (e: Exception) {
    Result(null, e)
}

data class Result<out T>(val success: T?, val failure: Exception?)

class Resource<out T>(val data: T?, val failure: Failure?)

class Failure(val errorCode: Int, val message: String) {

    fun isUnauthorized() = errorCode == RESULT_CODE_UNAUTHORIZED

}
