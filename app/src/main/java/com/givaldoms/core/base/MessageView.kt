package com.givaldoms.core.base

import android.content.Context
import android.support.annotation.StringRes
import android.widget.Toast
import com.givaldoms.R
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

/**
 * Created by givaldoms on 28/05/2018.
 */
interface MessageView {

    val mContext: Context

    fun showMessage(message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
    }

    fun showMessage(@StringRes message: Int) {
        Toast.makeText(mContext, mContext.getString(message), Toast.LENGTH_SHORT).show()
    }

    fun showNoInternetMessage() {
        showMessage(R.string.error_message_no_internet_connection)
    }

    fun showGenericErrorMessage() {
        showMessage(R.string.error_generic_message)
    }

    fun showGenericAlert(m: String,
                         okAction: () -> Any = {},
                         cancelAction: () -> Any = {}) {
        mContext.alert {
            message = m
            okButton {
                okAction()
            }

            if (cancelAction != {}) {
                negativeButton(android.R.string.cancel) {
                    cancelAction()
                }
            }

        }.show()
    }


}