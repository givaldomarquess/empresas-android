package com.givaldoms.core.base

/**
 * Created by givaldoms on 28/05/2018.
 */
interface BasePresenter<V> {

    var view: V

    fun stop()


}

interface HandlerPresenter {
    fun handlerException(ex: Exception)
}

