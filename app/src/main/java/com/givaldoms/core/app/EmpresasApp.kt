package com.givaldoms.core.app

import android.app.Application
import com.givaldoms.data.remoteDataSourceModule
import com.givaldoms.data.di.repositoryModule
import com.givaldoms.main.di.mainModule
import com.givaldoms.signin.di.signInModule
import org.koin.android.ext.android.startKoin

class EmpresasApp: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this,
                listOf(signInModule, mainModule, remoteDataSourceModule, repositoryModule))

    }

}