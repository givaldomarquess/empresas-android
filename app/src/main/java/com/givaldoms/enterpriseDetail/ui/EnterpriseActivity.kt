package com.givaldoms.enterpriseDetail.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem

import com.givaldoms.R
import com.givaldoms.core.Arguments
import com.givaldoms.core.Constants
import com.givaldoms.core.extensions.argument
import com.givaldoms.core.extensions.setImageUrl
import com.givaldoms.data.model.EnterpriseModel
import kotlinx.android.synthetic.main.activity_enterprise.*
import kotlinx.android.synthetic.main.item_enterprise.*

import java.util.Objects

class EnterpriseActivity : AppCompatActivity() {

    private val mEnterpriseModel: EnterpriseModel by argument(Arguments.ARG_ENTERPRISE_MODEL)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enterprise)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onStart() {
        super.onStart()

        enterpriseDescriptionTextView.text = mEnterpriseModel.description
        enterpriseToolbar.title = mEnterpriseModel.name
        setSupportActionBar(enterpriseToolbar)

        val photoUrl = "${Constants.WEB_SERVICE_BASE_URL}${mEnterpriseModel.photo}"
        enterpriseImageView.setImageUrl(photoUrl)
    }
}
