package com.givaldoms.main.di

import com.givaldoms.main.presentation.MainPresenter
import com.givaldoms.main.presentation.MainPresenterImpl
import org.koin.dsl.module.applicationContext

const val MAIN_VIEW = "main_view"

val mainModule = applicationContext {

    factory { MainPresenterImpl(get(), it[MAIN_VIEW]) as MainPresenter }
}

