package com.givaldoms.main.presentation

import android.os.Bundle
import com.givaldoms.core.extensions.withInternetStatus
import com.givaldoms.data.model.EnterpriseModel
import com.givaldoms.data.model.UserModel
import com.givaldoms.data.repository.EmpresasRepository
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch

class MainPresenterImpl(private val empresasRepository: EmpresasRepository,
                        override var view: MainView) : MainPresenter {

    override fun itemClick(enterpriseModel: EnterpriseModel, bundle: Bundle) {
        view.toEnterpriseDetail(enterpriseModel, bundle)
    }

    override fun getEnterprises(userModel: UserModel?, query: String) = withInternetStatus { isOnline ->
        if (!isOnline) {
            view.showNoInternetMessage()
            return@withInternetStatus
        }

        if (query.isBlank()) {
            handlerResult(emptyList())
            return@withInternetStatus
        }

        if (userModel == null) {
            view.toLogin()
            return@withInternetStatus
        }

        view.showProgress()


        launch(UI) {
            val result = async {
                empresasRepository.searchEnterpriseByName(userModel, query.trim())
            }.await()

            when {
                result.success == null -> view.showGenericErrorMessage()
                result.failure != null -> view.showMessage(result.failure.message ?: "")
                else -> handlerResult(result.success)
            }
            view.hideProgress()
        }

    }

    private fun handlerResult(list: List<EnterpriseModel>) {
        if (list.isEmpty()) {
            view.showNoResultMessage()
        } else {
            view.addResults(list)
        }
    }

    override fun stop() {
    }
}