package com.givaldoms.main.presentation

import android.os.Bundle
import com.givaldoms.core.base.BasePresenter
import com.givaldoms.core.base.BaseView
import com.givaldoms.core.base.LoadingView
import com.givaldoms.core.base.MessageView
import com.givaldoms.data.model.EnterpriseModel
import com.givaldoms.data.model.UserModel


interface MainPresenter: BasePresenter<MainView> {

    fun itemClick(enterpriseModel: EnterpriseModel, bundle: Bundle)

    fun getEnterprises(userModel: UserModel?, query: String)

}

interface MainView: BaseView<MainPresenter>, LoadingView, MessageView {

    fun showNoResultMessage()

    fun addResults(items: List<EnterpriseModel>)

    fun refreshResult()

    fun toEnterpriseDetail(enterpriseModel: EnterpriseModel, bundle: Bundle)

    fun toLogin()

}