package com.givaldoms.main.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.givaldoms.R
import com.givaldoms.core.Arguments
import com.givaldoms.core.Arguments.ARG_USER_MODEL
import com.givaldoms.core.extensions.argument
import com.givaldoms.core.extensions.onQueryTextSubmit
import com.givaldoms.data.model.EnterpriseModel
import com.givaldoms.data.model.UserModel
import com.givaldoms.enterpriseDetail.ui.EnterpriseActivity
import com.givaldoms.main.di.MAIN_VIEW
import com.givaldoms.main.presentation.MainPresenter
import com.givaldoms.main.presentation.MainView
import com.givaldoms.signin.ui.SignInActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity(), MainView,
        EnterpriseAdapter.OnAdapterItemClick {

    private val mUserModel: UserModel? by argument(ARG_USER_MODEL, null)

    private lateinit var mSearchView: SearchView


    override val presenter: MainPresenter by inject {
        mapOf(MAIN_VIEW to this)
    }

    override val mContext = this

    private var mEnterpriseAdapter: EnterpriseAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainToolbar?.title = getString(R.string.mainTitle)
        setSupportActionBar(mainToolbar)

        if (mUserModel == null) {
            startActivity<SignInActivity>()
            finishAffinity()
        }

    }

    override fun onStart() {
        super.onStart()
        mainSwipeRefreshLayout?.setOnRefreshListener {
            presenter.getEnterprises(mUserModel, mSearchView.query?.toString() ?: "")

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        mSearchView = menu.findItem(R.id.app_bar_search).actionView as SearchView
        mSearchView.queryHint = getString(R.string.search_hint)

        mSearchView.onQueryTextSubmit { query ->
            presenter.getEnterprises(mUserModel, query)
            true
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return true
    }

    override fun onAdapterItemClickListener(enterpriseModel: EnterpriseModel, bundle: Bundle) {
        presenter.itemClick(enterpriseModel, bundle)
    }

    override fun showProgress() {
        mainNoItemsMessageTextView.visibility = View.GONE
        mainSwipeRefreshLayout.isRefreshing = true
    }

    override fun hideProgress() {
        mainSwipeRefreshLayout.isRefreshing = false
    }

    override fun addResults(items: List<EnterpriseModel>) {
        mEnterpriseAdapter = EnterpriseAdapter(items, this)
        mainEnterprisesRecyclerView?.layoutManager = LinearLayoutManager(this)
        mainEnterprisesRecyclerView?.adapter = mEnterpriseAdapter

        mainEnterprisesRecyclerView.visibility = View.VISIBLE
        mainNoItemsMessageTextView.visibility = View.GONE
    }

    override fun showNoResultMessage() {
        mainNoItemsMessageTextView.text = getString(R.string.error_no_result_message)
        mainNoItemsMessageTextView.visibility = View.VISIBLE
        mainEnterprisesRecyclerView.visibility = View.GONE
    }

    override fun refreshResult() {
        mEnterpriseAdapter?.notifyDataSetChanged()
    }

    override fun toEnterpriseDetail(enterpriseModel: EnterpriseModel, bundle: Bundle) {
        val intent = intentFor<EnterpriseActivity>(Arguments.ARG_ENTERPRISE_MODEL to enterpriseModel)
        startActivity(intent, bundle)
    }

    override fun toLogin() {
        startActivity<SignInActivity>()
        finishAffinity()
    }
}
