package com.givaldoms.main.ui

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.givaldoms.R
import com.givaldoms.data.model.EnterpriseModel

class EnterpriseAdapter(private val mItems: List<EnterpriseModel>,
                        private val adapterClickListener: OnAdapterItemClick)
    : RecyclerView.Adapter<EnterpriseViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): EnterpriseViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.item_enterprise, viewGroup, false)
        return EnterpriseViewHolder(view)
    }

    override fun onBindViewHolder(enterpriseViewHolder: EnterpriseViewHolder, i: Int) {
        val item = mItems[i]
        enterpriseViewHolder.bind(item) {
            adapterClickListener.onAdapterItemClickListener(item, it)
        }
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    interface OnAdapterItemClick {

        fun onAdapterItemClickListener(enterpriseModel: EnterpriseModel, bundle: Bundle)

    }
}
