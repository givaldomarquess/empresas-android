package com.givaldoms.main.ui

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.givaldoms.R
import com.givaldoms.core.Constants
import com.givaldoms.core.extensions.setImageUrl
import com.givaldoms.data.model.EnterpriseModel
import org.jetbrains.anko.find

class EnterpriseViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val containerLayout: ConstraintLayout = itemView.find(R.id.itemEnterpriseLayout)
    private val imageView: ImageView = itemView.find(R.id.itemEnterpriseImageView)
    private val titleTextView: TextView = itemView.find(R.id.itemEnterpriseTitleTextView)
    private val subtitleTextView: TextView = itemView.find(R.id.itemEnterpriseSubtitleTextView)
    private val countryTextView: TextView = itemView.find(R.id.itemEnterpriseCountryTextView)

    internal fun bind(enterpriseModel: EnterpriseModel, itemClick: (Bundle) -> Unit) {
        titleTextView.text = enterpriseModel.name
        subtitleTextView.text = enterpriseModel.enterpriseType.type
        countryTextView.text = enterpriseModel.country

        containerLayout.setOnClickListener {
            val context = imageView.context as AppCompatActivity
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    context,
                    imageView,
                    context.getString(R.string.enterpriseImageTransitionName))
                    .toBundle() ?: Bundle()
            itemClick(options)
        }

        enterpriseModel.photo?.let {
            val photoUrl = "${Constants.WEB_SERVICE_BASE_URL}${enterpriseModel.photo}"
            imageView.setImageUrl(photoUrl)

        }

    }

}
