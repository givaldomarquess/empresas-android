package com.givaldoms.data

import com.givaldoms.core.Constants.WEB_SERVICE_BASE_URL
import com.givaldoms.data.DatasourceProperties.SERVER_URL
import com.givaldoms.data.repository.EmpresasDataSource
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val remoteDataSourceModule = applicationContext {
    bean { createOkHttpClient() }
    bean { createWebService<EmpresasDataSource>(get(), SERVER_URL) }
}

object DatasourceProperties {
    const val SERVER_URL = "$WEB_SERVICE_BASE_URL/api/v1/"
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String) = Retrofit
        .Builder()
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(okHttpClient)
        .build()
        .create(T::class.java)


fun createOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor).build()
}
