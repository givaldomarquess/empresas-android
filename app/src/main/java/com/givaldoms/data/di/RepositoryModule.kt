package com.givaldoms.data.di

import com.givaldoms.data.repository.EmpresasRepository
import com.givaldoms.data.repository.EmpresasRepositoryImpl
import org.koin.dsl.module.applicationContext

val repositoryModule = applicationContext{

    bean { EmpresasRepositoryImpl(get()) as EmpresasRepository}

}