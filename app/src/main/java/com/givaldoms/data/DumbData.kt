package com.givaldoms.data

import com.givaldoms.data.model.EnterpriseModel
import com.givaldoms.data.model.EnterpriseType
import com.givaldoms.data.model.UserModel
import java.util.*

object DumbData {

    fun dumbEnterpriseList(size: Int = 10): List<EnterpriseModel> {
        val l = ArrayList<EnterpriseModel>()

        (0..size).forEach { i ->
            l.add(EnterpriseModel("Nome $i", "Descrição $i", "País $i",
                    "https://picsum.photos/200", EnterpriseType("Tipo $i")))
        }
        return l
    }

    fun dumbUserModel() = UserModel(
            "dumbuser@domain.com",
            "dumb_client",
            "dumb_token")

}
