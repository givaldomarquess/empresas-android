package com.givaldoms.data.repository

import com.givaldoms.core.base.Failure
import com.givaldoms.core.base.Resource
import com.givaldoms.core.base.Result
import com.givaldoms.core.base.awaitResult
import com.givaldoms.data.model.EnterpriseModel
import com.givaldoms.data.model.UserModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async

interface EmpresasRepository {

    suspend fun searchEnterpriseByName(userModel: UserModel, nameQuery: String): Result<List<EnterpriseModel>>

    suspend fun doLogin(email: String, password: String): Resource<UserModel>
}

class EmpresasRepositoryImpl(private val empresasDataSource: EmpresasDataSource) : EmpresasRepository {

    override suspend fun doLogin(email: String, password: String) = async(CommonPool) {
        val result = empresasDataSource.doLogin(email, password).awaitResult()

        val headers = result.success?.headers()
        val user = UserModel(
                headers?.get("uid") ?: "",
                headers?.get("client") ?: "",
                headers?.get("access-token") ?: "")

        when {
            result.failure != null ->
                Resource(null, Failure(-1, "Ocorreu um erro desconhecido"))


            result.success?.errorBody() != null ->
                Resource(null, Failure(result.success.code(),
                        result.success.message() ?: ""))

            else -> {
                Resource(user, Failure(result.success?.code() ?: -1,
                        result.success?.message() ?: ""))
            }
        }

    }.await()

    override suspend fun searchEnterpriseByName(userModel: UserModel, nameQuery: String): Result<List<EnterpriseModel>> = async(CommonPool) {
        val result = empresasDataSource.getEnterpriseByName(
                userModel.uid,
                userModel.accessToken,
                userModel.client,
                nameQuery).awaitResult()

        when {
            result.success != null -> {
                val r = result.success.get("enterprises").asJsonArray
                val type = object : TypeToken<List<EnterpriseModel>>() {}.type
                val enterprises = Gson().fromJson<List<EnterpriseModel>>(r, type)
                Result(enterprises, null)

            }

            else -> Result(null, result.failure)
        }

    }.await()
}