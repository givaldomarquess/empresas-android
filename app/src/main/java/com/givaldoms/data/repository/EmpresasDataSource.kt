package com.givaldoms.data.repository

import com.google.gson.JsonObject
import kotlinx.coroutines.experimental.Deferred
import retrofit2.Response
import retrofit2.http.*

interface EmpresasDataSource {

    @FormUrlEncoded
    @POST("users/auth/sign_in")
    fun doLogin(@Field("email") email: String, @Field("password") password: String):
            Deferred<Response<JsonObject>>

    @GET("enterprises")
    fun getEnterpriseByName(@Header("uid") uid: String,
                            @Header("access-token") accessToken: String,
                            @Header("client") client: String,
                            @Query("name") companyName: String):
            Deferred<JsonObject>

}
