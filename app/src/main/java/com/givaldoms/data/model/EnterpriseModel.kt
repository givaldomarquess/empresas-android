package com.givaldoms.data.model

import android.os.Parcelable

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EnterpriseModel(
        @field:SerializedName("enterprise_name")
        val name: String,
        @field:SerializedName("description")
        val description: String,
        @field:SerializedName("country")
        val country: String,
        @field:SerializedName("photo")
        val photo: String?,
        @field:SerializedName("enterprise_type")
        var enterpriseType: EnterpriseType) : Parcelable
