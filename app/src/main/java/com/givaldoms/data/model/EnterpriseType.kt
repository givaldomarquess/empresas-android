package com.givaldoms.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EnterpriseType(
        @SerializedName("enterprise_type_name") val type: String)
    : Parcelable
