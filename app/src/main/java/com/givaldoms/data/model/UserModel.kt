package com.givaldoms.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserModel(
        val uid: String,
        val client: String,
        val accessToken: String) : Parcelable